#!/usr/bin/env ruby

def main
    game = true
    count = 0
    
    r = Random.new
    find = r.rand(100)

    puts "Bienvenue dans le jeu du juste prix !"

    while game do
        print "Choisir un nombre: "
        begin
            resp = Integer(gets)

            if resp >= 0 && resp <= 100

                count += 1
                if resp > find
                    puts "Le nombre choisit est trop grand !"
                elsif resp < find
                    puts "Le nombre choisit est trop petit !"
                else
                    puts "Bravo vous avez trouvé le nombre : #{find}"
                    puts "Nombre d'essais total : #{count}"
                    game = false
                end

            else
                puts "Merci de choisir un nombre entre 1 et 100 !"
            end
        rescue ArgumentError
            puts "Merci de choisir un nombre entier valide !"
        end
    end
end

if __FILE__ == $0
    begin
        main
    rescue Interrupt
        puts "\nMerci d'avoir joué !"
    end
end