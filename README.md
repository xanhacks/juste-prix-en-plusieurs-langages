# Juste prix

Voici le jeu très connu du juste prix écrit sous différents langages de programmation !

## Languages
- Python3
- Python2
- Java
- C
- Ruby
- ShellScript
- Perl
- Go
- Rust
- C++

## Next
- Javascript
- C#

## Soon
- ASM
- PHP
