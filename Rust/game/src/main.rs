use std::io;
use std::io::Write;
use rand::Rng;

fn main() {
    let mut game = true;
    let mut count = 0;
    let find = rand::thread_rng().gen_range(1, 101);

    println!("Bienvenue dans le jeu du juste prix !");

    while game {
        print!("Choisir un nombre: ");
        io::stdout().flush().unwrap();

        let mut resp = String::new();
        io::stdin().read_line(&mut resp).expect("Failed to read line");
        let resp: i32 = resp.trim().parse().expect("Please give me correct string number!");

        if resp >= 0 && resp <= 100 {
            count += 1;

            if resp > find {
                println!("Le nombre choisit est trop grand !");
            } else if resp < find {
                println!("Le nombre choisit est trop petit !");
            } else {
                println!("Bravo vous avez trouvé le nombre : {}", find);
                println!("Nombre d'essais total : {}", count);
                game = false;
            }
        } else {
            println!("Merci de choisir un nombre entre 1 et 100 !");
        }
    }

}