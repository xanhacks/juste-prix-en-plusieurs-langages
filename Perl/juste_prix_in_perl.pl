#!/usr/bin/perl

use warnings;

sub juste_prix {
    $game = 1;
    $count = 0;
    $find = int(rand(101));

    print "Bienvenue dans le jeu du juste prix !\n";

    while ($game){
	    print "Choisir un nombre: ";
	    $resp = <STDIN>;

	    if ($resp >= 0 and $resp <= 100){
		    
		    $count += 1;
		    if ($resp > $find){
			    print "Le nombre choisit est trop grand !\n";
		    } elsif ($resp < $find){
			    print "Le nombre choisit est trop petit !\n";
		    } else {
			    print "Bravo vous avez trouvé le nombre : $find\n";
			    print "Nombre d'essais total : $count\n";
			    $game = 0;
		    }
	    } else {
		    print "Merci de choisir un nombre entre 1 et 100 !\n";
	    }
    }
}

juste_prix();
