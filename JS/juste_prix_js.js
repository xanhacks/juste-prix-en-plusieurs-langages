function main(){
	let game = true;
    let count = 0;
    
    let find = Math.floor((Math.random() * 100) + 1);
    
    console.log("Bienvenue dans le jeu du juste prix !");
    
    while (game){
		let resp = process.stdin.setEncoding('utf-8');
        
        if (resp >= 0 && resp <= 100){

            count++;

        	if (resp > find){
            	console.log("Le nombre choisit est trop grand !");
            } else if (resp < find){
            	console.log("Le nombre choisit est trop petit !");
            } else {
            	console.log("Bravo vous avez trouvé le nombre : " + find);
                console.log("Nombre d'essais total : " + count);
                game = false;
            }
        
        } else {
        	console.log("Merci de choisir un nombre entre 1 et 100 !");
        }

    }
}

main();


