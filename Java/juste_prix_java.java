import java.util.Scanner;

class juste_prix_java{

    public static void main(String[] args){
        boolean game = true;
        int resp;
        int count = 0;
        int find = (int) (Math.random() * 100);

        System.out.println("Bienvenue dans le jeu du juste prix !");

        while (game){
            try {
                Scanner scan = new Scanner(System.in);
                System.out.print("Choisir un nombre: ");
                resp = scan.nextInt();

                if (resp >= 0 && resp <= 100){
                    
                    count++;
                    if (resp > find){
                        System.out.println("Le nombre choisit est trop grand !");
                    } else if (resp < find) {
                        System.out.println("Le nombre choisit est trop petit !");
                    } else {
                        System.out.println("Bravo vous avez trouvé le nombre : " + find);
                        System.out.println("Nombre d'essais total : " + count);
                        game = false;
                    }
                } else {
                    System.out.println("Merci de choisir un nombre entre 1 et 100 !");
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Merci de choisir un nombre entier valide !");
            }
        }

        /*
        while game:
            try:
                resp = int(input("Choisir un nombre: "))

                if resp >= 0 and resp <= 100:

                    if resp > find:
                        print("Le nombre choisit est trop grand !")
                    elif resp < find:
                        print("Le nombre choisit est trop petit !")
                    else:
                        print(f"Bravo vous avez trouvé le nombre : {find}")
                        game = False
                
                else:
                    print("Merci de choisir un nombre entre 1 et 100 !")


            except ValueError:
                print("Merci de choisir un nombre entier valide !")
            */
    }

}