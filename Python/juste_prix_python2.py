#!/usr/bin/env python2
# coding: utf8
import random

def juste_prix():
    game = True
    count = 0
    find = random.randint(0, 100)

    print "Bienvenue dans le jeu du juste prix !"

    while game:
        try:
            resp = int(input("Choisir un nombre: "))

            if resp >= 0 and resp <= 100:

                count += 1
                if resp > find:
                    print "Le nombre choisit est trop grand !"
                elif resp < find:
                    print "Le nombre choisit est trop petit !"
                else:
                    print "Bravo vous avez trouvé le nombre : " + str(find)
                    print "Nombre d'essais total : " + str(count)
                    game = False
            
            else:
                print "Merci de choisir un nombre entre 1 et 100 !"


        except (NameError, SyntaxError):
            print "Merci de choisir un nombre entier valide !"
			

if __name__ == "__main__":
    try:
        juste_prix()
    except KeyboardInterrupt:
        print "\nMerci d'avoir joué !"

