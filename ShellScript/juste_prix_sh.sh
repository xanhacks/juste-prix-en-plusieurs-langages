#!/bin/sh

main()
{
GAME=true
COUNT=0
FIND=$(shuf -i 0-100 -n 1)

echo "Bienvenue dans le jeu du juste prix !"

while [ $GAME = true ]
do
    read -p "Choisir un nombre: " RESP

    if [ -n "$RESP" ] && [ "$RESP" -eq "$RESP" ] 2>/dev/null; then

        if (( $RESP >= 0)) && (( $RESP <= 100 )); then

            COUNT=$((COUNT+1))

            if (( $RESP > $FIND )); then
                echo "Le nombre choisit est trop grand !"
            elif (( $RESP < $FIND )); then
                echo "Le nombre choisit est trop petit !"
            else
                echo "Bravo vous avez trouvé le nombre : $FIND"
                echo "Nombre d'essais total : $COUNT"
                GAME=false
            fi
        
        else
            echo "Merci de choisir un nombre entre 1 et 100 !"
        fi

    else
        echo "Merci de choisir un nombre entier valide !"
    fi
done
}

main