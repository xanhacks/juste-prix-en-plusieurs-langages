#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>


void main(void){
    bool game = true;
    int count = 0;
    int resp, find;

    printf("Bienvenue dans le jeu du juste prix !\n");

    srand(time(NULL));
    find = rand() % 101;

    while (game){
        
        printf("Choisir un nombre: ");
        scanf("%d", &resp);

        if (resp >= 0 && resp <= 100){
            
            count++;
            if (resp > find){
                printf("Le nombre choisit est trop grand !\n");
            } else if (resp < find) {
                printf("Le nombre choisit est trop petit !\n");
            } else {
                printf("Bravo vous avez trouvé le nombre : %d\n", find);
                printf("Nombre d'essais total : %d\n", count);
                game = false;
            }
        } else {
            printf("Merci de choisir un nombre entre 1 et 100 !\n");
        }

    }

}