package main

import (
    "bufio"
    "fmt"
    "os"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

func main(){
	var game int
	var count int
	find := rand.New(rand.NewSource(time.Now().UnixNano())).Intn(100)

	fmt.Println("Bienvenue dans le jeu du juste prix !")

	for game < 1 {

		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Choisir un nombre: ")
		resp, _ := reader.ReadString('\n')
		respInt, _ := strconv.Atoi(strings.TrimSpace(resp))

		if respInt >= 0 && respInt <= 100 {

			count++
			if respInt > find {
				fmt.Println("Le nombre choisit est trop grand !")
			} else if respInt < find{
				fmt.Println("Le nombre choisit est trop petit !")
			} else {
				fmt.Println(fmt.Sprintf("Bravo vous avez trouvé le nombre : %d", find))
				fmt.Println(fmt.Sprintf("Nombre d'essais total : %d", count))
				game = 1
			}
		
		} else {
			fmt.Println("Merci de choisir un nombre entre 1 et 100 !")
		}
	}
}